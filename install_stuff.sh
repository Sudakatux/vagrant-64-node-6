#!/usr/bin/env bash
 
sudo locale-gen en_US.UTF-8
sudo update-locale LANG=en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8
 
sudo apt-get update
printf "\n*****************************\n"
printf "* Installing Essential Stuff*\n "
printf  "*****************************\n"
sudo apt-get install -y build-essential curl libxslt1-dev libxml2-dev python-software-properties mc vim curl git

#sudo apt-add-repository ppa:chris-lea/redis-server

#sudo apt-get update

curl --silent --location https://deb.nodesource.com/setup_6.x | sudo bash -
sudo apt-get update
sudo apt-get install --yes nodejs
sudo npm install -g webpack webpack-dev-server

printf "Everything installed"



